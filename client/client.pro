TEMPLATE = app
TARGET = openrecipes
QT += qml quick quickcontrols2 sql svg widgets xml
include( ../linking.pri )
include( ../common.pri )

HEADERS += \
	src/Backend.h \
	src/DataStructs.h \
	src/EncryptedItem.h \
	src/Globals.h \
	src/Ingredient.h \
	src/PRandom.h \
	src/QREncoder.h \
	src/Recipe.h \
	src/RecvSyncKeyAsync.h \
	src/SecureClientConnection.h \
	src/SendSyncKeyAsync.h \
	src/SqlBackend.h \
	src/SynchronizeAsync.h

SOURCES += \
	src/Backend.cpp \
	src/DataStructs.cpp \
	src/EncryptedItem.cpp \
	src/Globals.cpp \
	src/Ingredient.cpp \
	src/PRandom.cpp \
	src/QREncoder.cpp \
	src/Recipe.cpp \
	src/RecvSyncKeyAsync.cpp \
	src/SecureClientConnection.cpp \
	src/SendSyncKeyAsync.cpp \
	src/SqlBackend.cpp \
	src/SynchronizeAsync.cpp \
	src/main.cpp

RESOURCES += client.qrc

unix:!android: PKGCONFIG += libqrencode

win32 {
	warning("TODO: add libqrencode")

	HEADERS += \
		src/WindowsSelfUpdate.h

	SOURCES += \
		src/WindowsSelfUpdate.cpp
}

android {
	HEADERS += src/Jni.h
	SOURCES += src/Jni.cpp
	LIBS += -lqrencode
	ANDROID_EXTRA_LIBS += \
		$$(OPENRECIPES_ANDROID_SYSROOT)/lib/libsodium.so \
		$$(OPENRECIPES_ANDROID_SYSROOT)/lib/libqrencode.so
	QMAKE_SUBSTITUTES += ../android-sources/AndroidManifest.xml.in
}

TRANSLATIONS += intl/client_de.ts
lupdate_only {SOURCES += qml/*.qml}

android {
	#There are more translation files available there, but this one seems to suffice
	QMAKE_POST_LINK = $$QMAKE_MKDIR $$shell_path($$PWD/../android-sources/assets/translations) && $$QMAKE_COPY $$shell_path($$[QT_INSTALL_TRANSLATIONS]/qt_de.qm) $$shell_path($$PWD/../android-sources/assets/translations)
}

qtPrepareTool(LRELEASE, lrelease)
system($$LRELEASE $$_PRO_FILE_)|error("Can't run lrelease")
QMAKE_DISTCLEAN += $$PWD/intl/client_*.qm

unix {
	system($$QMAKE_COPY $$PWD/img/icon.svg $$PWD/img/openrecipes.svg)
	QMAKE_DISTCLEAN += $$PWD/img/openrecipes.svg
}

target.path = /usr/bin
icon.path = /usr/share/pixmaps
icon.files = $$PWD/img/openrecipes.svg
desktop.path = /usr/share/applications
desktop.files = $$PWD/openrecipes.desktop
INSTALLS += target
!android: INSTALLS += icon desktop

win32 {
	RC_FILE = windows_icon.rc
	CONFIG += windeployqt
}

lessThan(QT_MINOR_VERSION, 10): warning("You seem to use a Qt version prior to 5.10.0. The client will build with this version, but NOT run.")
