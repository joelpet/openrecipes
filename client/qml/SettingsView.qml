/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import org.jschwab.recipes 1.0

Page {
	ModalDialog {
		id: sendSyncKeyDialog
		standardButtons: Dialog.NoButton

		title: qsTr("Sending sync key…")

		ColumnLayout {
			anchors.fill: parent

			ProgressBar {
				Layout.alignment: Qt.AlignHCenter
				indeterminate: true
			}

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("Please wait while your sync key is send to the server.")
				wrapMode: Text.Wrap
			}
		}

		//onRejected: backend.stopSendSyncKey(); //TODO implement

		Connections {
			target: backend
			onSendSyncKeyDone: {
				if (success) {
					showSyncKeyDialog.open();
					sendSyncKeyDialog.accept();
				} else {
					sendSyncKeyDialog.reject();
					sendSyncKeyFailedDialog.open();
				}
			}
		}
	}

	ModalDialog {
		id: sendSyncKeyFailedDialog
		standardButtons: Dialog.Ok

		title: qsTr("Sending sync key failed")

		ColumnLayout {
			anchors.fill: parent

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("Sending your sync key failed.");
				wrapMode: Text.Wrap
			}
		}
	}

	ModalDialog {
		id: showSyncKeyDialog
		standardButtons: Dialog.Close

		property int openTime
		property int remTime

		title: qsTr("Your sync key")

		ScrollView {
			anchors.fill: parent
			clip: true

			Flickable {
				boundsBehavior: Flickable.StopAtBounds

				ColumnLayout {
					anchors.fill: parent

					Label {
						Layout.maximumWidth: mainWindow.width - 50
						Layout.alignment: Qt.AlignHCenter
						text: qsTr("Scan the QR Code with your other device:")
						wrapMode: Text.Wrap
					}

					Image {
						id: qrcode
						source: "image://QRCode/" + backend.syncKeyHex
						cache: false
						Layout.alignment: Qt.AlignHCenter
						sourceSize.width: Math.min(mainWindow.width - 50, 250)
						sourceSize.height: Math.min(mainWindow.height - 50, 250)
					}

					Label {
						Layout.maximumWidth: qrcode.width
						Layout.alignment: Qt.AlignHCenter
						text: qsTr("If scaning the QR Code is not possible, you can also type in the key by hand:")
						wrapMode: Text.Wrap
					}

					TextField {
						Layout.maximumWidth: qrcode.width
						Layout.alignment: Qt.AlignHCenter
						text: backend.syncKeyHex
						font.family: "Monospace"
						wrapMode: Text.Wrap
						readOnly: true
					}

					Label {
						Layout.maximumWidth: qrcode.width
						Layout.alignment: Qt.AlignHCenter
						text: qsTr("(Your sync key will be available on the server for %1:%2 minutes.)").arg(Math.floor(showSyncKeyDialog.remTime / 60.)).arg(showSyncKeyDialog.remTime % 60)
						wrapMode: Text.Wrap
					}
				}
			}
		}

		Timer {
			id: timer
			interval: 1000
			repeat: true
			running: false
			triggeredOnStart: true
			onTriggered: {
				showSyncKeyDialog.remTime = 1800 - (new Date().getTime() / 1000 - showSyncKeyDialog.openTime);
				if (showSyncKeyDialog.remTime < 0) showSyncKeyDialog.reject();
			}
		}

		onOpened: {
			openTime = new Date().getTime() / 1000;
			timer.start();
		}

		onClosed: timer.stop()
	}

	ModalDialog {
		id: enterSyncKeyDialog
		standardButtons: Globals.mobile ? Dialog.Ok : Dialog.Ok | Dialog.Cancel

		title: qsTr("Enter sync key")

		ColumnLayout {
			anchors.fill: parent

			Button {
				Layout.alignment: Qt.AlignHCenter
				onClicked: {
					enterSyncKeyDialog.reject();
					backend.requestQRCode();
				}
				text: qsTr("Scan QR Code")
				highlighted: true
				visible: Globals.mobile
			}

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: Globals.mobile ? qsTr("If scaning the QR Code is not possible, you can also type in the key by hand:") : qsTr("Enter the sync key from the device you wish to sync with:")
				wrapMode: Text.Wrap
			}

			TextArea {
				Layout.maximumWidth: mainWindow.width - 20
				Layout.preferredWidth: mainWindow.width - 20

				id: key
				height: 200
				placeholderText: qsTr("Key")
				font.family: "Monospace"
				wrapMode: TextEdit.Wrap
			}
		}

		onAccepted: {
			backend.setupSyncFromKey(key.text)
			//TODO open dialog and sync
		}
	}

	ModalDialog {
		id: recvSyncKeyDialog
		standardButtons: Dialog.NoButton

		title: qsTr("Receiving sync key…")

		ColumnLayout {
			anchors.fill: parent

			ProgressBar {
				Layout.alignment: Qt.AlignHCenter
				indeterminate: true
			}

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("Please wait while your sync key is fetched from the server.")
				wrapMode: Text.Wrap
			}
		}

		//onRejected: backend.stopRecvSyncKey(); //TODO implement

		Connections {
			target: backend
			onRecvSyncKeyDone: {
				if (success) {
					recvSyncKeyDialog.accept();
				} else {
					recvSyncKeyDialog.reject();
					recvSyncKeyFailedDialog.open();
				}
			}
		}
	}

	ModalDialog {
		id: recvSyncKeyFailedDialog
		standardButtons: Dialog.Ok

		title: qsTr("Receiving sync key failed")

		ColumnLayout {
			anchors.fill: parent

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("Receiving your sync key failed.");
				wrapMode: Text.Wrap
			}
		}
	}

	ModalDialog {
		id: advancedSyncSettingsDialog
		standardButtons: Dialog.Ok

		title: qsTr("Advanced sync settings")

		ColumnLayout {
			anchors.fill: parent

			CheckBox {
				checked: backend.useCustomSyncServer
				text: qsTr("Use a custom sync server")
				onToggled: backend.useCustomSyncServer = checked
			}

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				text: qsTr("Sync server address:")
				wrapMode: Text.Wrap
				visible: backend.useCustomSyncServer
			}

			TextField {
				Layout.leftMargin: 10
				Layout.rightMargin: 10
				Layout.fillWidth: Globals.mobile
				placeholderText: qsTr("domain.tld")
				text: backend.customSyncServerAddr
				onTextEdited: backend.customSyncServerAddr = text
				visible: backend.useCustomSyncServer
			}

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				text: qsTr("Sync server port:")
				wrapMode: Text.Wrap
				visible: backend.useCustomSyncServer
			}

			TextField {
				Layout.leftMargin: 10
				Layout.rightMargin: 10
				Layout.fillWidth: Globals.mobile
				validator: IntValidator{
					bottom: 0
					top: 65536
				}
				placeholderText: qsTr("1234")
				text: backend.customSyncServerPort
				onTextEdited: backend.customSyncServerPort = parseInt(text)
				visible: backend.useCustomSyncServer
				inputMethodHints: Qt.ImhDigitsOnly
			}

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				text: qsTr("Sync server key:")
				wrapMode: Text.Wrap
				visible: backend.useCustomSyncServer
			}

			TextField {
				Layout.leftMargin: 10
				Layout.rightMargin: 10
				Layout.fillWidth: Globals.mobile

				height: 200
				placeholderText: qsTr("Key")
				font.family: "Monospace"
				text: backend.customSyncServerKeyHex
				onTextEdited: backend.customSyncServerKeyHex = text
				visible: backend.useCustomSyncServer
			}
		}
	}

	ModalDialog {
		id: aboutDialog
		standardButtons: Globals.mobile ? Dialog.NoButton : Dialog.Close

		title: qsTr("About %1").arg(Qt.application.name)

		ScrollView {
			anchors.fill: parent
			clip: true

			Flickable {
				boundsBehavior: Flickable.StopAtBounds

				ColumnLayout {
					anchors.fill: parent

					MediumLabel {
						Layout.maximumWidth: mainWindow.width - 50
						Layout.alignment: Qt.AlignHCenter
						text: qsTr("%1 %2").arg(Qt.application.name).arg(Qt.application.version)
						wrapMode: Text.Wrap
					}

					Label {
						Layout.maximumWidth: mainWindow.width - 50
						Layout.alignment: Qt.AlignHCenter
						text: qsTr("Author: Johannes Schwab")
						wrapMode: Text.Wrap
					}

					Label {
						Layout.maximumWidth: mainWindow.width - 50
						Layout.alignment: Qt.AlignHCenter
						text: "This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.\n\nYou should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>."
						wrapMode: Text.Wrap
					}
				}
			}
		}
	}

	header: ToolBar {
		RowLayout {
			anchors.fill: parent
			BigLabel {
				text: qsTr("Settings")
				horizontalAlignment: Qt.AlignHCenter
				verticalAlignment: Qt.AlignVCenter
				Layout.fillWidth: true
			}
		}
	}

	ColumnLayout {
		anchors.top: parent.top
		anchors.bottom: parent.bottom
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.left: Globals.mobile ? parent.left : undefined
		anchors.right: Globals.mobile ? parent.right : undefined

		MediumLabel {
			Layout.topMargin: 5
			Layout.alignment: Qt.AlignHCenter
			text: qsTr("Synchronization settings")
		}

		Button {
			Layout.alignment: Qt.AlignHCenter
			onClicked: function() {
				if (!backend.syncAvailable) {
					backend.setupSync();
				}
				backend.sendSyncKey();
				sendSyncKeyDialog.open();
			}
			text: qsTr("Add device to sync with this one")
			highlighted: Globals.mobile
			Layout.fillWidth: true
			Layout.leftMargin: 10
			Layout.rightMargin: 10
		}

		Button {
			Layout.alignment: Qt.AlignHCenter
			onClicked: enterSyncKeyDialog.open()
			text: qsTr("Sync with another device")
			visible: !backend.syncAvailable
			highlighted: Globals.mobile
			Layout.fillWidth: true
			Layout.leftMargin: 10
			Layout.rightMargin: 10
		}

		Button {
			Layout.alignment: Qt.AlignHCenter
			onClicked: backend.removeSyncSettings()
			text: qsTr("Reset sync")
			visible: backend.syncAvailable
			Layout.fillWidth: true
			Layout.leftMargin: 10
			Layout.rightMargin: 10
		}

		ToolButton {
			Layout.alignment: Qt.AlignRight
			onClicked: advancedSyncSettingsDialog.open()
			text: qsTr("Advanced Settings")
			visible: !backend.syncAvailable
			highlighted: Globals.mobile
			Layout.rightMargin: 10
		}

		Label {
			Layout.alignment: Qt.AlignHCenter
			text: qsTr("You are currently using the custom sync server \"%1:%2\"").arg(backend.customSyncServerAddr).arg(backend.customSyncServerPort)
			wrapMode: Text.Wrap
			visible: backend.useCustomSyncServer && backend.syncAvailable
			Layout.leftMargin: 10
			Layout.rightMargin: 10
		}			

		Item {
			Layout.fillHeight: true
		}

		Button {
			Layout.alignment: Qt.AlignHCenter
			onClicked: aboutDialog.open()
			text: qsTr("About %1").arg(Qt.application.name)
			flat: true
			Layout.bottomMargin: 5
		}
	}	

	footer: RowLayout {
		Item {
			Layout.fillWidth: true
		}

		Button {
			text: qsTr("Back")
			onClicked: stackView.pop();
			Layout.bottomMargin: 5
			Layout.rightMargin: 5
		}
		visible: !Globals.mobile
	}
}
