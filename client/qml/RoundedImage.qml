/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.10
import QtGraphicalEffects 1.0

Item {
	id: roundedImage
	property string source
	property int fillMode

	implicitHeight: image.implicitHeight
	implicitWidth: image.implicitWidth


	Image {
		id: image
		anchors.fill: parent
		source: roundedImage.source
		fillMode: roundedImage.fillMode
		visible: false
	}

	OpacityMask {
		anchors.fill: image
		source: image
		maskSource: Item {
			width: image.width
			height: image.height

			Rectangle {
				anchors.centerIn: parent
				width: Math.min(image.width, image.paintedWidth)
				height: Math.min(image.height, image.paintedHeight)
				radius: 8
			}
			visible: false
		}
	}
}
