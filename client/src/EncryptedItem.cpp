/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "EncryptedItem.h"
#include "DataStructs.h"
#include "SqlBackend.h"

#include <sodium.h>
#include <QtEndian>
#include <cassert>
#include <cstring>

EncryptedItem::EncryptedItem(const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce)
:
	id(id),
	cipher(cipher),
	nonce(nonce)
{
	if (id.size() != 16) throw IERROR("Id has invalid size");
	if (static_cast<size_t>(cipher.size()) < crypto_secretbox_MACBYTES) throw IERROR("Cipher to short");
	if (static_cast<size_t>(nonce.size()) != crypto_secretbox_NONCEBYTES) throw IERROR("Nonce has invalid size");
	data.resize(cipher.size() - crypto_secretbox_MACBYTES);
	if (crypto_secretbox_open_easy(
				reinterpret_cast<unsigned char*>(data.data()),
				reinterpret_cast<const unsigned char*>(cipher.constData()),
				cipher.size(),
				reinterpret_cast<const unsigned char*>(nonce.constData()),
				reinterpret_cast<const unsigned char*>(SqlBackend::getSyncKey().constData())
	) != 0) throw IERROR("Can't decrypt item");
	if (data.size() < 4 + 8 + 16) throw IERROR("Data to short");
	const quint64 itemDbStruct = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(data.constData() + 4));
	if (itemDbStruct > 1) throw IERROR("Can't handle db struct, please update");
	if (std::memcmp(data.constData() + 4 + 8, id.constData(), 16) != 0) throw IERROR("Encrypted id differs from plaintext id");
}

EncryptedItem::EncryptedItem(const QByteArray &id, const QByteArray &data)
:
	id(id),
	data(data)
{
	nonce.resize(crypto_secretbox_NONCEBYTES);
	cipher.resize(data.size() + crypto_secretbox_MACBYTES);
	randombytes_buf(reinterpret_cast<unsigned char*>(nonce.data()), nonce.size());
	if (crypto_secretbox_easy(
				reinterpret_cast<unsigned char*>(cipher.data()),
				reinterpret_cast<const unsigned char*>(data.constData()),
				data.size(),
				reinterpret_cast<const unsigned char*>(nonce.constData()),
				reinterpret_cast<const unsigned char*>(SqlBackend::getSyncKey().constData())
	) != 0) throw IERROR("Can't encrypt item");
}

QByteArray EncryptedItem::getId() const {
	return id;
}

QByteArray EncryptedItem::getCipher() const {
	return cipher;
}

QByteArray EncryptedItem::getNonce() const {
	return nonce;
}

EncryptedItem::Type EncryptedItem::getType() const {
	if (std::strncmp(data.constData(), "RECI", 4) == 0) {
		return Type::Recipe;
	} else {
			throw IERROR("Unknown type");
	}
}

EncryptedRecipe::EncryptedRecipe(const EncryptedItem &ei)
:
	EncryptedItem(ei)
{}

RecipeData EncryptedRecipe::getData() const {
	if (data.size() < 4 + 8 + 16 + 5 * 8) throw IERROR("Data to short");
	assert(std::strncmp(data.constData(), "RECI", 4) == 0);
	RecipeData r;
	r.id = id;
	size_t pos = 4 + 8 + 16; //"RECI", dbStruct, id
	r.portions = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(data.constData() + pos));
	pos += 8;
	const quint64 nameSize = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(data.constData() + pos));
	pos += 8;
	const quint64 imageSize = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(data.constData() + pos));
	pos += 8;
	const quint64 instructionSize = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(data.constData() + pos));
	pos += 8;
	const quint64 numberOfIngredients = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(data.constData() + pos));
	pos += 8;
	if (static_cast<size_t>(data.size()) < 4 + 8 + 16 + 5 * 8 + numberOfIngredients * 3 * 8) throw IERROR("Data to short");
	r.ingredients.resize(numberOfIngredients);
	for (auto &i : r.ingredients) {
		i.count = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(data.constData() + pos));
		pos += 8;
	}
	std::vector<std::pair<quint64, quint64>> ingrUnitArticleSize;
	size_t ingrUnitArticleSizeTotal = 0;
	ingrUnitArticleSize.reserve(numberOfIngredients);
	for (size_t i = 0; i < numberOfIngredients; ++i) {
		ingrUnitArticleSize.emplace_back(
				qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(data.constData() + pos)),
				qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(data.constData() + pos + 8))
		);
		pos += 16;
		ingrUnitArticleSizeTotal += ingrUnitArticleSize.back().first + ingrUnitArticleSize.back().second;
	}

	if (static_cast<size_t>(data.size()) != pos + nameSize + imageSize + instructionSize + ingrUnitArticleSizeTotal) throw IERROR("Data for recipe has invalid length");
	r.name = QString::fromUtf8(data.constData() + pos, nameSize);
	pos += nameSize;
	r.image = QString::fromUtf8(data.constData() + pos, imageSize);
	pos += imageSize;
	r.instruction = QString::fromUtf8(data.constData() + pos, instructionSize);
	pos += instructionSize;
	for (size_t i = 0; i < numberOfIngredients; ++i) {
		r.ingredients[i].unit = QString::fromUtf8(data.constData() + pos, ingrUnitArticleSize[i].first);
		pos += ingrUnitArticleSize[i].first;
		r.ingredients[i].article = QString::fromUtf8(data.constData() + pos, ingrUnitArticleSize[i].second);
		pos += ingrUnitArticleSize[i].second;
	}
	assert(pos == static_cast<size_t>(data.size()));
	return r;
}

EncryptedItem EncryptedItem::fromRecipeData(const RecipeData& rd) {
	size_t totalSize = 4 + 8 + 16 + 8 + 8; //RECI, dbStruct, id, portions, numberOfIngredients
	totalSize += rd.ingredients.size() * 3 * 8; //ingrCount, ingrUnitSize, ingrArticleSize
	std::vector<std::pair<QByteArray, QByteArray>> ingrUnitArticleUtf8;
	ingrUnitArticleUtf8.reserve(rd.ingredients.size());
	for (const auto &i : rd.ingredients) {
		ingrUnitArticleUtf8.emplace_back(i.unit.toUtf8(), i.article.toUtf8());
		totalSize += ingrUnitArticleUtf8.back().first.size() + ingrUnitArticleUtf8.back().second.size();
	}
	const QByteArray nameUtf8 = rd.name.toUtf8();
	const QByteArray imageUtf8 = rd.image.toUtf8();
	const QByteArray instructionUtf8 = rd.instruction.toUtf8();
	totalSize += nameUtf8.size() + imageUtf8.size() + instructionUtf8.size();
	QByteArray data("RECI");
	data.reserve(totalSize);
	data.resize(4 + 8);
	*reinterpret_cast<quint64*>(data.data() + 4) = qToBigEndian<quint64>(1u); //dbStruct
	data += rd.id;
	size_t pos = data.size();
	data.resize(data.size() + 5 * 8 + 3 * rd.ingredients.size() * 8);
	*reinterpret_cast<quint64*>(data.data() + pos) = qToBigEndian<quint64>(rd.portions);
	pos += 8;
	*reinterpret_cast<quint64*>(data.data() + pos) = qToBigEndian<quint64>(nameUtf8.size());
	pos += 8;
	*reinterpret_cast<quint64*>(data.data() + pos) = qToBigEndian<quint64>(imageUtf8.size());
	pos += 8;
	*reinterpret_cast<quint64*>(data.data() + pos) = qToBigEndian<quint64>(instructionUtf8.size());
	pos += 8;
	*reinterpret_cast<quint64*>(data.data() + pos) = qToBigEndian<quint64>(rd.ingredients.size());
	pos += 8;
	for (const auto &i : rd.ingredients) {
		*reinterpret_cast<quint64*>(data.data() + pos) = qToBigEndian<quint64>(i.count);
		pos += 8;
	}
	for (const auto &i : ingrUnitArticleUtf8) {
		*reinterpret_cast<quint64*>(data.data() + pos) = qToBigEndian<quint64>(i.first.size());
		pos += 8;
		*reinterpret_cast<quint64*>(data.data() + pos) = qToBigEndian<quint64>(i.second.size());
		pos += 8;
	}
	assert(pos == static_cast<size_t>(data.size()));
		
	data += nameUtf8;
	data += imageUtf8;
	data += instructionUtf8;
	for (const auto &i : ingrUnitArticleUtf8) {
		data += i.first;
		data += i.second;
	}
	return EncryptedItem(rd.id, data);
}
