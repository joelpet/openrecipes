/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DATA_STRUCTS_H
#define DATA_STRUCTS_H

#include <QByteArray>
#include <QString>
#include <vector>

struct IngredientData {
	QString unit, article;
	quint64 count;
	bool operator==(int id) const;
};

struct RecipeData {
	QByteArray id;
	QString name, image, instruction;
	quint64 portions;
	std::vector<IngredientData> ingredients;
	bool operator==(const QByteArray &id) const;
};

struct SyncKeyData {
	QByteArray publicKey, secretKey;
};

#endif //DATA_STRUCTS_H
