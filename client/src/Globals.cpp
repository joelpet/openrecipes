/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Globals.h"
#include <QVersionNumber>

Globals::Globals(QObject *parent)
:
	QObject(parent)
{}

bool Globals::isMobile() {
#ifdef Q_OS_ANDROID
	return true;
#else
	return false;
#endif
};

QString Globals::getAccentColor() {
	return "#FF5722";
}

unsigned int Globals::getRecipesListImageHeight() {
	return isMobile() ? 55 : 45;
}

QString Globals::getDefaultServerHostName() {
	return "api.openrecipes.jschwab.org";
}

quint16 Globals::getDefaultServerPort() {
	return 44556u;
}

QByteArray Globals::getDefaultServerKey() {
	return QByteArray::fromHex("FF1EDE8932E9E944471AA2CF9ED361A1D58905E4109DA052417BE156638EB4E9");
}

QString Globals::getMinimumQtVersionStr() {
	return getMinimumQtVersion().toString();
}

QVersionNumber Globals::getMinimumQtVersion() {
	return QVersionNumber(5, 10, 0);
}

QByteArray Globals::getUpdateSigningKey() {
	return QByteArray::fromHex("5C86B2FD68F44CE7446CBFF5F4589B5985F39B725AE852DCF15E4CE6EB9A2E26");
}
