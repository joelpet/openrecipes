/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SecureConnection.h"
#include "FixedSizeNetworkPackage.h"
#include "InternalError.h"
#include "SqlBackendBase.h"

#include <QTcpSocket>
#include <QTimer>
#include <QtEndian>
#include <cassert>
#include <cstring>
#include <ctime>

SecureConnection::SecureConnection(QObject *parent, QTcpSocket *socket, Role role, bool authSelf)
:
	Connection(parent),
	connectionTimer(new QTimer(this)),
	role(role),
	authSelf(authSelf),
	state(State::WaitingForRemoteKeyexchangePublicKey),
	socket(socket)
{
	assert(role != Role::Server || authSelf);
	qDebug() << "Initializing SecureConnection";
	socket->setParent(this);
	connect(socket, &QTcpSocket::disconnected, this, [this]() {
			emit disconnected();
			state = State::Disconnected;
	});
	connect(socket, &QTcpSocket::bytesWritten, this, [this](quint64) {emit bytesWritten();});

	connectionTimer->setSingleShot(true);
	connect(connectionTimer, &QTimer::timeout, this, [this]() {
			qWarning() << "SecureConnection timed out";
			onError();
			}
	);
	connectionTimer->start(establishTimeOut);

	if (socket->state() == QAbstractSocket::ConnectedState) {
		sendOwnKeyexchangePublicKey();
	} else {
		connect(socket, &QTcpSocket::connected, this, [this]() {sendOwnKeyexchangePublicKey();});
	}

	newFsNetPackage(sizeof(publicKey));
}

SecureConnection::~SecureConnection() {
	sodium_memzero(secretKey, sizeof(secretKey));
	sodium_memzero(receiveKey, sizeof(receiveKey));
	sodium_memzero(sendKey, sizeof(sendKey));
	sodium_memzero(&receiveState, sizeof(receiveState));
	sodium_memzero(&sendState, sizeof(sendState));
}

void SecureConnection::newFsNetPackage(quint64 size) {
	fsNetPackage.reset(new FixedSizeNetworkPackage(size));
	connect(fsNetPackage.get(), &FixedSizeNetworkPackage::complete, this, &SecureConnection::onNetworkPackageCompleted);
	fsNetPackage->read(new SocketConnection(fsNetPackage.get(), socket));
}

void SecureConnection::onNetworkPackageCompleted() {
	try {
		switch (state) {
			case State::WaitingForRemoteKeyexchangePublicKey:
				assert(fsNetPackage->getData().size() == sizeof(publicKey));
				qDebug() << "Got remote publicKey for key exchange";
				genSessionKeys(fsNetPackage->getData());
				sendAuthOrNoau();
				if (authSelf) sendSessionKeysSig();
				state = State::WaitingForAuthOrNoau;
				newFsNetPackage(4);
				break;
			case State::WaitingForAuthOrNoau:
				assert(fsNetPackage->getData().size() == 4);
				qDebug() << "Got Auth or Noau";
				if (fsNetPackage->getData() == QByteArray("AUTH")) {
					state = State::WaitingForSessionKeysSig;
					newFsNetPackage(sessionKeysSigSize);
				} else if (fsNetPackage->getData() == QByteArray("NOAU")) {
					if (role == Role::Client) throw IERROR("Server refuses to authenticate himselfe");
					sendEncryptionHeader();
					state = State::WaitingForEncryptionHeader;
					newFsNetPackage(encryptionHeaderSize);
				} else {
					throw IERROR("Got invalid request code");
				}
				break;
			case State::WaitingForSessionKeysSig:
				assert(fsNetPackage->getData().size() == sessionKeysSigSize);
				qDebug() << "Got remote syncPublicKey and session key signatures";
				verifyKeySigs(fsNetPackage->getData());
				sendEncryptionHeader();
				state = State::WaitingForEncryptionHeader;
				newFsNetPackage(encryptionHeaderSize);
				break;
			case State::WaitingForEncryptionHeader:
				assert(fsNetPackage->getData().size() == encryptionHeaderSize);
				if (crypto_secretstream_xchacha20poly1305_init_pull(&receiveState, reinterpret_cast<const unsigned char*>(fsNetPackage->getData().constData()), receiveKey) != 0) {
					onError();
					return;
				}
				sodium_memzero(receiveKey, sizeof(receiveKey));
				qDebug() << "Done initializing SecureConnection";
				state = State::CryptoEstablished;
				fsNetPackage.reset();
				connect(socket, &QTcpSocket::readyRead, this, &SecureConnection::onSocketReadyRead);
				emit cryptoEstablished();
				connectionTimer->start(idleTimeOut);
				onSocketReadyRead();
				break;
			case State::CryptoEstablished:
			case State::Disconnected:
				assert(false);
		}
	} catch (const InternalError &e) {
		onError();
	}
}

void SecureConnection::sendOwnKeyexchangePublicKey() {
	crypto_kx_keypair(publicKey, secretKey);
	QByteArray b;
	b.resize(sizeof(publicKey));
	std::memcpy(b.data(), publicKey, sizeof(publicKey));
	qDebug() << "Sending own publicKey for key exchange";
	tryWrite(b);
}

void SecureConnection::onError() {
	disconnect(socket, nullptr, this, nullptr);
	socket->deleteLater();
	state = State::Disconnected;
	emit disconnected();
}

size_t SecureConnection::bytesAvailable() const {
	return plaintextBuffer.getSize();
}

void SecureConnection::tryWrite(const QByteArray &data) {
	if (socket->write(data) != data.size()) throw IERROR("Can't send response, closing connection");
}

void SecureConnection::sendAuthOrNoau() {
	if (authSelf) {
		tryWrite(QByteArray("AUTH"));
	} else {
		tryWrite(QByteArray("NOAU"));
	}
}

void SecureConnection::genSessionKeys(const QByteArray &remoteKeyexchangePublicKey) {
	assert(remoteKeyexchangePublicKey.size() == sizeof(publicKey));
	switch (role) {
		case Role::Server:
			if (crypto_kx_server_session_keys(receiveKey, sendKey, publicKey, secretKey, reinterpret_cast<const unsigned char*>(remoteKeyexchangePublicKey.constData())) != 0) throw IERROR("Got invalid public key");
			break;
		case Role::Client:
			if (crypto_kx_client_session_keys(receiveKey, sendKey, publicKey, secretKey, reinterpret_cast<const unsigned char*>(remoteKeyexchangePublicKey.constData())) != 0) throw IERROR("Got invalid public key");
			break;
	}
	qDebug() << "Session keys generated";
	sodium_memzero(secretKey, sizeof(secretKey));
}

void SecureConnection::sendSessionKeysSig() {
	qDebug() << "Sending own syncPublicKey and session key signatures";
	tryWrite(SqlBackendBase::getSyncPublicKey());

	const quint64 timestampBE = qToBigEndian<quint64>(std::time(nullptr));
	QByteArray timestampBEBA;
	timestampBEBA.resize(8);
	*reinterpret_cast<quint64*>(timestampBEBA.data()) = timestampBE;
	tryWrite(timestampBEBA);

	unsigned char toHash[2 * crypto_kx_SESSIONKEYBYTES + 8];
	std::memcpy(toHash, receiveKey, crypto_kx_SESSIONKEYBYTES);
	std::memcpy(toHash + crypto_kx_SESSIONKEYBYTES, sendKey, crypto_kx_SESSIONKEYBYTES);
	*reinterpret_cast<quint64*>(toHash + 2 * crypto_kx_SESSIONKEYBYTES) = timestampBE;

	unsigned char hash[crypto_generichash_BYTES];
	crypto_generichash(hash, sizeof(hash), toHash, sizeof(toHash), nullptr, 0);

	const QByteArray syncSecretKey = SqlBackendBase::getSyncSecretKey();
	assert(syncSecretKey.size() == crypto_sign_SECRETKEYBYTES);
	QByteArray sig;
	sig.resize(crypto_sign_BYTES);
	crypto_sign_detached(reinterpret_cast<unsigned char*>(sig.data()), nullptr, hash, sizeof(hash), reinterpret_cast<const unsigned char*>(syncSecretKey.constData()));
	tryWrite(sig);
}

void SecureConnection::verifyKeySigs(const QByteArray &data) {
	assert(data.size() == sessionKeysSigSize);
	const unsigned char *const remotePublicKey = reinterpret_cast<const unsigned char*>(data.constData());
	const quint64 remoteTimestampBE = *reinterpret_cast<const quint64*>(data.constData() + crypto_sign_PUBLICKEYBYTES);
	const unsigned char *const sig = remotePublicKey + crypto_sign_PUBLICKEYBYTES + 8;

	const quint64 timestamp = std::time(nullptr);
	if (llabs(timestamp - qFromBigEndian<quint64>(remoteTimestampBE)) > 60 * 30) throw IERROR("Remote timestamp differs more then 30 minutes");
	qDebug() << "Remote timestamp is good";

	unsigned char toHash[2 * crypto_kx_SESSIONKEYBYTES + 8];
	std::memcpy(toHash, sendKey, crypto_kx_SESSIONKEYBYTES);
	std::memcpy(toHash + crypto_kx_SESSIONKEYBYTES, receiveKey, crypto_kx_SESSIONKEYBYTES);
	*reinterpret_cast<quint64*>(toHash + 2 * crypto_kx_SESSIONKEYBYTES) = remoteTimestampBE;

	unsigned char hash[crypto_generichash_BYTES];
	crypto_generichash(hash, sizeof(hash), toHash, sizeof(toHash), nullptr, 0);

	if (crypto_sign_verify_detached(sig, hash, sizeof(hash), remotePublicKey) != 0) throw IERROR("Got invalid signatures");
	qDebug() << "Signature is valid";

	if (!verifyRemotePublicKey(data.left(crypto_sign_PUBLICKEYBYTES))) throw IERROR("Got invalid public key");
	qDebug() << "Remote syncPublicKey is valid";
}

void SecureConnection::sendEncryptionHeader() {
	QByteArray header;
	header.resize(crypto_secretstream_xchacha20poly1305_HEADERBYTES);
	if (crypto_secretstream_xchacha20poly1305_init_push(&sendState, reinterpret_cast<unsigned char*>(header.data()), sendKey) != 0) throw IERROR("Can't init push");
	sodium_memzero(sendKey, sizeof(sendKey));
	tryWrite(header);
}

void SecureConnection::decrypt() {
	assert(encryptedBuffer.getSize() == encryptedBlockSize);
	const QByteArray data = encryptedBuffer.read();
	unsigned char tag;
	QByteArray plaintext;
	plaintext.resize(blockSize);
	if (crypto_secretstream_xchacha20poly1305_pull(&receiveState, reinterpret_cast<unsigned char*>(plaintext.data()), nullptr, &tag, reinterpret_cast<const unsigned char*>(data.constData()), data.size(), nullptr, 0) != 0) throw IERROR("Can't decrypt data");
	paddedBuffer += plaintext;
	if (tag == crypto_secretstream_xchacha20poly1305_TAG_PUSH) unpad();
}

void SecureConnection::unpad() {
	QByteArray data = paddedBuffer.read();
	size_t unpaddedSize;
	if (sodium_unpad(&unpaddedSize, reinterpret_cast<const unsigned char*>(data.constData()), data.size(), blockSize) != 0) throw IERROR("Incorrect padding");
	data.truncate(unpaddedSize);
	plaintextBuffer += data;
	emit readyRead();
}

void SecureConnection::onSocketReadyRead() {
	if (state == State::Disconnected) return;
	assert(state == State::CryptoEstablished);
	while (socket->bytesAvailable()) {
		QByteArray data = socket->read(encryptedBlockSize - encryptedBuffer.getSize());
		encryptedBuffer += data;
		if (encryptedBuffer.getSize() == encryptedBlockSize) {
			try {
				decrypt();
			} catch (const InternalError &e) {
				onError();
			}
		}
	}
	connectionTimer->start(idleTimeOut);
}

void SecureConnection::write(const QByteArray &data) {
	size_t paddedSize;
	QByteArray paddedData;
	paddedData.reserve(data.size() + blockSize);
	paddedData += data;
	paddedData.resize(data.size() + blockSize); //TODO enough?
	if (sodium_pad(&paddedSize, reinterpret_cast<unsigned char*>(paddedData.data()), data.size(), blockSize, paddedData.size()) != 0) throw IERROR("Can't pad data");
	paddedData.truncate(paddedSize);
	assert(paddedData.size() % blockSize == 0);

	const size_t numberOfBlocks = paddedData.size() / blockSize;
	QByteArray tmp;
	tmp.resize(encryptedBlockSize);
	for (size_t i = 0; i < numberOfBlocks; ++i) {
		if (crypto_secretstream_xchacha20poly1305_push(&sendState, reinterpret_cast<unsigned char*>(tmp.data()), nullptr, reinterpret_cast<const unsigned char*>(paddedData.constData()) + i * blockSize, blockSize, nullptr, 0, (i == numberOfBlocks - 1 ? crypto_secretstream_xchacha20poly1305_TAG_PUSH : 0)) != 0) throw IERROR("Can't encrypt data");
		tryWrite(tmp);
	}
	connectionTimer->start(idleTimeOut);
}
	
QByteArray SecureConnection::read(size_t upToBytes) {
	return plaintextBuffer.read(upToBytes);
}
