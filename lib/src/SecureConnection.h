/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SECURE_CONNECTION_H
#define SECURE_CONNECTION_H

#include "Connection.h"
#include "FixedSizeNetworkPackage.h"
#include "ByteArrayBuffer.h"

#include <sodium.h>
#include <QObject>
#include <QByteArray>
#include <memory>

class QTcpSocket;
class QTimer;

/*!
 * \brief Handles a secure (encrypted and authenticated) tcp connection.
 */
class SecureConnection : public Connection {
	Q_OBJECT

	public:
		/*!
		 * \brief Used to determine if this is the server or the client.
		 */
		enum class Role {
			Server,
			Client
		};

	private:
		/*!
		 * \brief Possible states this site can be in.
		 */
		enum class State {
			WaitingForRemoteKeyexchangePublicKey,
			WaitingForAuthOrNoau,
			WaitingForSessionKeysSig,
			WaitingForEncryptionHeader,
			CryptoEstablished,
			Disconnected
		};

		/*!
		 * \brief Used to let the connection time out.
		 */
		QTimer *connectionTimer;

		/*!
		 * \brief Whether or not this is the server or client end.
		 */
		const Role role;

		/*!
		 * \brief Whether or not this instance will authenticate itself.
		 */
		const bool authSelf;

		/*!
		 * \brief The current state.
		 */
		State state;

		/*!
		 * \brief Secret key for key exchange.
		 */
		unsigned char secretKey[crypto_kx_SECRETKEYBYTES];

		/*!
		 * \brief Public key for key exchange.
		 */
		unsigned char publicKey[crypto_kx_PUBLICKEYBYTES];

		/*!
		 * \brief Encryption key for receiving data.
		 */
		unsigned char receiveKey[crypto_kx_SESSIONKEYBYTES];

		/*!
		 * \brief Encryption key for sending data.
		 */
		unsigned char sendKey[crypto_kx_SESSIONKEYBYTES];

		/*!
		 * \brief Encryption state for receiving data.
		 */
		crypto_secretstream_xchacha20poly1305_state receiveState;

		/*!
		 * \brief Encryption state for sending data.
		 */
		crypto_secretstream_xchacha20poly1305_state sendState;

		/*!
		 * \brief Buffer to hold encrypted data.
		 */
		ByteArrayBuffer encryptedBuffer;

		/*!
		 * \brief Buffer to hold decrypted, but still padded data.
		 */
		ByteArrayBuffer paddedBuffer;

		/*!
		 * \brief Buffer to hold plaintext data.
		 */
		ByteArrayBuffer plaintextBuffer;

		/*!
		 * \brief Current network package.
		 *
		 * Only used while the connection is established.
		 */
		std::unique_ptr<FixedSizeNetworkPackage> fsNetPackage;

		/*!
		 * \brief Write to socket.
		 * \exception InternalError in case writing fails.
		 */
		void tryWrite(const QByteArray &data);

		/*!
		 * \brief Reset fsNetPackage and connect the signals for the new package.
		 * \param size number of bytes to read with the new package
		 */
		void newFsNetPackage(quint64 size);

		/*!
		 * \brief Send "AUTH" or "NOAU", depending on authSelf.
		 * \exception InternalError
		 */
		void sendAuthOrNoau();

		/*!
		 * \brief Send own keyexchange public key.
		 * \exception InternalError
		 */
		void sendOwnKeyexchangePublicKey();

		/*!
		 * \brief Generate session keys.
		 * \exception InternalError
		 */
		void genSessionKeys(const QByteArray &remoteKeyexchangePublicKey);

		/*!
		 * \brief Send the session keys signature.
		 * \exception InternalError
		 */
		void sendSessionKeysSig();

		/*!
		 * \brief Send the session keys signature.
		 * \exception InternalError
		 */
		void sendEncryptionHeader();

		/*!
		 * \brief Verify the session keys signature received from the remote site.
		 * \exception InternalError
		 */
		void verifyKeySigs(const QByteArray &data);

		/*!
		 * \brief Decrypt the data in encryptedBuffer and write the result to paddedBuffer.
		 * \exception InternalError
		 *
		 * encryptedBuffer.size() must equal encryptedBlockSize.
		 */
		void decrypt();

		/*!
		 * \brief Unpad the data in paddedBuffer and write the result to plaintextBuffer.
		 * \exception InternalError
		 */
		void unpad();

		/*!
		 * \brief The size of one plain text block.
		 */
		constexpr static size_t blockSize = 128;

		/*!
		 * \brief The size of one encrypted block.
		 */
		constexpr static size_t encryptedBlockSize = blockSize + crypto_secretstream_xchacha20poly1305_ABYTES;

		/*!
		 * \brief Time to establish the connection (in milliseconds).
		 */
		constexpr static unsigned int establishTimeOut = 10000;

		/*!
		 * \brief Time after with the connection is closed when idle (in milliseconds).
		 */
		constexpr static unsigned int idleTimeOut = 5000;

		/*!
		 * \brief Size of the session keys signature package.
		 */
		constexpr static size_t sessionKeysSigSize = crypto_sign_PUBLICKEYBYTES + 8 + crypto_sign_BYTES;

		/*!
		 * \brief Size of the encyption header package.
		 */
		constexpr static size_t encryptionHeaderSize = crypto_secretstream_xchacha20poly1305_HEADERBYTES;

	private slots:
		/*!
		 * \brief Handles a completed network package.
		 * \sa fsNetPackage
		 */
		void onNetworkPackageCompleted();

		/*!
		 * \brief Handles new data on the socket.
		 * \sa socket
		 */
		void onSocketReadyRead();

	protected:
		/*!
		 * \brief The socket to use.
		 */
		QTcpSocket *const socket;

		/*!
		 * \brief Handle an error.
		 *
		 * Dissconnects and deletes the socket and emits disconnected()
		 */
		void onError();

		/*!
		 * \brief Verify the remote public key.
		 * \param key the key to verify
		 * \return wether or not the key is valid
		 */
		virtual bool verifyRemotePublicKey(const QByteArray &key) = 0;

	public:
		/*!
		 * \brief Initialized a new secure connection.
		 * \param parent parent
		 * \param socket the socket to use
		 * \param role determines whether or not this is the server or client end
		 * \param authSelf determines whether or not this instance will authenticate itself
		 * \exception InternalError
		 *
		 * SecureConnection will take ownership of the socket.
		 * When the connection has been established, cryptoEstablished() is emitted.
		 * If role == Role::Server, authSelf must allways be true.
		 */
		SecureConnection(QObject *parent, QTcpSocket *socket, Role role, bool authSelf);

		/*!
		 * \brief Destructor.
		 */
		~SecureConnection();

		/*!
		 * \brief Write data to the connection.
		 * \param data data to write
		 * \exception InternalError
		 *
		 * When the data has been written, bytesWritten is emitted.
		 */
		void write(const QByteArray &data);

		/*!
		 * \brief Number of bytes available to read.
		 * \return number of bytes that are available to read
		 */
		size_t bytesAvailable() const override;

		/*!
		 * \brief Read up to upToBytes bytes.
		 * \param upToBytes maximum number of bytes to read
		 * \return data
		 */
		QByteArray read(size_t upToBytes) override;

	signals:
		/*!
		 * \brief Emitted when the connection is ready to read and write data.
		 */
		void cryptoEstablished();

		/*!
		 * \brief Emitted when the bytes passed to write have been written.
		 */
		void bytesWritten();

};

#endif //SECURE_CONNECTION_H
