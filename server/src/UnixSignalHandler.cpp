/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "UnixSignalHandler.h"
#include <InternalError.h>

#include <sys/socket.h>
#include <QSocketNotifier>
#include <unistd.h>
#include <csignal>
#include <cassert>

UnixSignalHandler::UnixSignalHandler() {
	if (socketpair(AF_UNIX, SOCK_STREAM, 0, sigCloseGentlyFd) != 0) throw IERROR("Can't create sockets");
	if (socketpair(AF_UNIX, SOCK_STREAM, 0, sigPrintInfoFd) != 0) throw IERROR("Can't create sockets");

	QSocketNotifier *sigCloseGentlyNot = new QSocketNotifier(sigCloseGentlyFd[1], QSocketNotifier::Read, this);
	QSocketNotifier *sigPrintInfoNot = new QSocketNotifier(sigPrintInfoFd[1], QSocketNotifier::Read, this);

	connect(sigCloseGentlyNot, &QSocketNotifier::activated, this, [this, sigCloseGentlyNot](int fd) {
			assert(fd == sigCloseGentlyFd[1]);
			sigCloseGentlyNot->setEnabled(false);
			char sig;
			read(fd, &sig, 1);
			qInfo() << "Closing gently as requested";
			emit closeGently();
			sigCloseGentlyNot->setEnabled(true);
	});
	connect(sigPrintInfoNot, &QSocketNotifier::activated, this, [this, sigPrintInfoNot](int fd) {
			assert(fd == sigPrintInfoFd[1]);
			sigPrintInfoNot->setEnabled(false);
			char sig;
			read(fd, &sig, 1);
			qDebug() << "Print info requested";
			emit printInfo();
			sigPrintInfoNot->setEnabled(true);
	});

	struct sigaction siga;
	siga.sa_handler = UnixSignalHandler::sigCloseGentlyHandler;
	sigemptyset(&siga.sa_mask);
	siga.sa_flags = SA_RESTART;
	if (sigaction(SIGTERM, &siga, nullptr) != 0) throw IERROR("Can't set signal hanlder");
	if (sigaction(SIGINT, &siga, nullptr) != 0) throw IERROR("Can't set signal hanlder");
	siga.sa_handler = UnixSignalHandler::sigPrintInfoHandler;
	if (sigaction(SIGUSR1, &siga, nullptr) != 0) throw IERROR("Can't set signal hanlder");
	signal(SIGHUP, SIG_IGN);
}

void UnixSignalHandler::sigCloseGentlyHandler(int sig) {
	Q_UNUSED(sig);
	constexpr char a = 1;
	write(sigCloseGentlyFd[0], &a, 1);
}

void UnixSignalHandler::sigPrintInfoHandler(int sig) {
	Q_UNUSED(sig);
	constexpr char a = 1;
	write(sigPrintInfoFd[0], &a, 1);
}

int UnixSignalHandler::sigCloseGentlyFd[2];
int UnixSignalHandler::sigPrintInfoFd[2];
