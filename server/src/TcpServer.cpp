/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SqlBackend.h"
#include "TcpServer.h"
#include "TcpConnection.h"

#include <QTcpServer>
#include <ctime>

TcpServer::TcpServer(const TcpServer::Config &conf)
:
	tcpServer(new QTcpServer(this)),
	startupTime(std::time(nullptr)),
	closingGently(false)
{
	if (startupTime == -1) throw IERROR("Can't get the time");
	const quint16 port = conf.port;
	tcpServer->listen(QHostAddress::Any, port);
	connect(tcpServer, &QTcpServer::newConnection, this, &TcpServer::onNewConnection);
	qDebug() << "Server started listening at port" << port;
}

void TcpServer::onNewConnection() {
	static_assert(sizeof(quint64) >= sizeof(time_t));
	TcpConnection *con = new TcpConnection(this, tcpServer->nextPendingConnection(), startupTime);
	connect(con, &TcpConnection::closed, this, &TcpServer::onTcpConnectionClosed, Qt::QueuedConnection);
}

void TcpServer::onCloseGently() {
	qDebug() << "Closing server...";
	closingGently = true;
	tcpServer->close();
	onTcpConnectionClosed();
}

void TcpServer::onTcpConnectionClosed() {
	if (closingGently) {
		if (findChildren<TcpConnection*>(QString(), Qt::FindDirectChildrenOnly).isEmpty()) {
			qDebug() << "Server closed";
			emit closed();
		}
	}
}

void TcpServer::onPrintInfo() {
	QString publicKey;
	unsigned long numberOfTodaysUsers;
	try {
		publicKey = QString::fromLatin1(SqlBackend::getSyncPublicKey().toHex()).toUpper();
		numberOfTodaysUsers = SqlBackend::getNumberOfTodaysUsers();
	} catch (const SqlNoResult &e) {
		qWarning() << "Error while gathering infos";
		return;
	}
	qInfo() << "OpenRecipesServer state:\n"
		"\t-Running since" << asctime(localtime(&startupTime)) << "\n"
		"\t-Listening on port" << tcpServer->serverPort() << "\n"
		"\t-Public key is" << publicKey << "\n"
		"\t-At the moment" << TcpConnection::getNumberOfLoggedInUsers() << "users are logged in\n"
		"\t-Today" << numberOfTodaysUsers << "users have been active\n";
}

