/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "BackendWorker.h"
#include "DbItem.h"
#include "SqlBackend.h"

#include <QDebug>
#include <sodium.h>

BackendWorker::BackendWorker()
:
	stop(false)
{}

void BackendWorker::onClose() {
	qDebug() << "Stopping BackendWorker...";
	stopMutex.lock();
	stop = true;
	stopMutex.unlock();
	waitCond.wakeAll();
}

void BackendWorker::run() {
	std::function<void (BackendWorker*)> func;
	while(true) {
		bool stopCpy;
		stopMutex.lock();
		stopCpy = stop;
		stopMutex.unlock();
		if (stopCpy) break;

		tasksMutex.lock();
		if (tasks.empty()) waitCond.wait(&tasksMutex);
		if (tasks.empty()) {
			tasksMutex.unlock();
			continue;
		}
		func = tasks.back();
		tasks.pop_back();
		tasksMutex.unlock();
		func(this);
	}
	qDebug() << "BackendWorker stopped";
}

void BackendWorker::appendTask(const std::function<void (BackendWorker*)> &task) {
	tasksMutex.lock();
	tasks.push_front(task);
	tasksMutex.unlock();
	waitCond.wakeAll();
}

//TODO catch InternalError

GetIdsRes BackendWorker::getIds(quint64 userId) {
	try {
		return std::make_pair(true, SqlBackend::getIds(userId));
	} catch (const SqlNoResult &e) {
		qCritical() << "Can't get ids for userId " << userId;
		return std::make_pair(false, std::vector<QByteArray>());
	}
}

bool BackendWorker::addOrUpdate(const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce, quint64 userId) {
	try {
		SqlBackend::addOrUpdate(id, cipher, nonce, userId);
		return true;
	} catch (const SqlNoResult &e) {
		qCritical() << "Can't add or update item for user " << userId;
		return false;
	}
}

bool BackendWorker::deleteEntry(const QByteArray &id, quint64 userId) {
	try {
		SqlBackend::deleteEntry(id, userId);
		return true;
	} catch (const SqlNoResult &e) {
		qCritical() << "Can't delete item for user " << userId;
		return false;
	}
}

GetChangedRes BackendWorker::getChanged(quint64 timestamp, quint64 userId) {
	try {
		return std::make_pair(true, SqlBackend::getChanged(timestamp, userId));
	} catch (const SqlNoResult &e) {
		qCritical() << "Can't get changed for user " << userId;
		return std::make_pair(false, std::vector<DbItem>());
	}
}

GetSyncKeyRes BackendWorker::getSyncKey(const QByteArray &id) {
	try {
		return std::make_pair(true, SqlBackend::getAndDelSyncKey(id));
	} catch (const SqlNoResult &e) {
		qCritical() << "Can't get sync key";
		return std::make_pair(false, std::make_pair(QByteArray(), QByteArray()));
	}
}

bool BackendWorker::setSyncKey(const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce) {
	try {
		SqlBackend::addSyncKey(id, cipher, nonce);
		return true;
	} catch (const SqlNoResult &e) {
		qCritical() << "Can't add sync key";
		return false;
	}
}

std::list<std::function<void (BackendWorker*)>> BackendWorker::tasks;
QMutex BackendWorker::tasksMutex;
QWaitCondition BackendWorker::waitCond;
