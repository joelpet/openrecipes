/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REQUEST_H
#define REQUEST_H

#include <NetworkPackage.h>

#include <QtGlobal>
#include <QByteArray>
#include <QString>
#include <memory>

class Request : public NetworkPackage {
	public:
		enum class Type {
			GetProtocolVersion,
			GetStartupTime,
			GetIds,
			AddOrUpdate,
			Delete,
			GetChanged,
			Close,
			GetSyncKey,
			SetSyncKey
		};

	public:
		static std::shared_ptr<Request> getRequest(const QByteArray &keyword);
		Request& operator+=(const QByteArray &data);

		virtual Type getType() const = 0;

		constexpr static quint64 keywordSize = 5;
};

class RequestWithData : public Request {
	public:
		quint64 bytesNeeded() const final;
};

class RequestWithoutData : public Request {
	public:
		quint64 bytesNeeded() const final;
};

class RequestWithTwoParams : public RequestWithData {
	protected:
		std::pair<QByteArray, QByteArray> getParams(unsigned char offset) const;
};

class RequestGetProtocolVersion : public RequestWithoutData {
	public:
		Type getType() const final;
};

class RequestGetStartupTime : public RequestWithoutData {
	public:
		Type getType() const final;
};

class RequestGetIds : public RequestWithoutData {
	public:
		Type getType() const final;
};

class RequestIdCipherNonce : public RequestWithTwoParams {
	public:
		struct CipherAndNonce {
			const QByteArray cipher, nonce;
			CipherAndNonce(const QByteArray &cipher, const QByteArray &nonce)
				: cipher(cipher), nonce(nonce) {}
		};

		QByteArray getId() const;
		CipherAndNonce getCipherAndNonce() const;
};

class RequestAddOrUpdate : public RequestIdCipherNonce {
	public:
		Type getType() const final;
};

class RequestDelete : public RequestWithData {
	public:
		Type getType() const final;
		QByteArray getId() const;
};

class RequestGetChanged : public RequestWithData {
	public:
		Type getType() const final;
		quint64 getTimestamp() const;
};

class RequestClose : public RequestWithoutData {
	public:
		Type getType() const final;
};

class RequestGetSyncKey : public RequestWithData {
	public:
		Type getType() const final;
		QByteArray getId() const;
};

class RequestSetSyncKey : public RequestIdCipherNonce {
	public:
		Type getType() const final;
};

#endif //REQUEST_H
