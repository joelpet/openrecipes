/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DB_ITEM
#define DB_ITEM

#include <QByteArray>

struct DbItem {
	QByteArray id, cipher, nonce;
	DbItem(const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce) : id(id), cipher(cipher), nonce(nonce) {};
};

#endif //DB_ITEM
