/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UNIX_SIGNAL_HANDLER_H
#define UNIX_SIGNAL_HANDLER_H

#include <QObject>

class UnixSignalHandler : public QObject {
	Q_OBJECT
	
	private:
		static int sigCloseGentlyFd[2];
		static int sigPrintInfoFd[2];
		
		static void sigCloseGentlyHandler(int sig);
		static void sigPrintInfoHandler(int sig);

	public:
		UnixSignalHandler();

	signals:
		void closeGently();
		void printInfo();
};

#endif //UNIX_SIGNAL_HANDLER_H
