TEMPLATE = app
CONFIG += console
TARGET = openrecipesserver
QT += core network sql
QT -= gui
include( ../linking.pri )
include( ../common.pri )

SOURCES +=  \
			src/Async.cpp \
			src/BackendWorker.cpp \
			src/Request.cpp \
			src/RequestParser.cpp \
			src/SecureServerConnection.cpp \
			src/SqlBackend.cpp \
			src/TcpConnection.cpp \
			src/TcpServer.cpp \
			src/UnixSignalHandler.cpp \
			src/main.cpp

HEADERS +=  \
			src/Async.h \
			src/BackendWorker.h \
			src/DbItem.h \
			src/HelperDefs.h \
			src/Request.h \
			src/RequestParser.h \
			src/SecureServerConnection.h \
			src/SqlBackend.h \
			src/TcpConnection.h \
			src/TcpServer.h \
			src/UnixSignalHandler.h

target.path = /usr/bin
INSTALLS += target
