package org.jschwab.openrecipes;

import android.content.ContentProvider;
import android.os.ParcelFileDescriptor;
import android.net.Uri;
import android.content.ContentValues;
import android.os.Bundle;
import android.database.Cursor;
import java.io.FileNotFoundException;
import java.io.IOException;
import android.content.UriMatcher;
import java.lang.Thread;
import java.io.FileWriter;
import android.util.Log;
import java.util.List;
import android.database.MatrixCursor;
import android.provider.OpenableColumns;
import org.jschwab.openrecipes.Helpers;

public class XmlProvider extends ContentProvider {
	private static Thread writeThread;

	@Override
	public boolean onCreate() {
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		Log.d("OpenRecipes", "XmlParser::query");
		Matcher m = new Matcher();
		if (m.match(uri) == Matcher.RECIPE_XML) {
			List<String> seg = uri.getPathSegments();
			if (seg.size() != 2 || !seg.get(0).equals("recipe_xml")) return null;
			String recipeName = Helpers.getRecipeName(seg.get(1));
			if (recipeName.length() == 0) recipeName = new String("unnamed");
			MatrixCursor c = new MatrixCursor(new String[] {"_id", "_data", "mime_type", OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE});
			c.addRow(new Object[] {0, uri, "openrecipes/recipe_xml", recipeName + ".openrecipes", 0 /*recipeXml.length()*/});
			return c;
		} else {
			Log.i("OpenRecipes", "Got invalid uri");
			return null;
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		return 0;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		Log.d("OpenRecipes", "XmlParser::getType for " + uri.toString());
		Matcher m = new Matcher();
		if (m.match(uri) == Matcher.RECIPE_XML) {
			Log.d("OpenRecipes", "return type openrecipes/recipe_xml");
			return "openrecipes/recipe_xml";
		} else {
			Log.d("OpenRecipes", "no type match");
			return null;
		}
	}
	
	@Override
	public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
		Log.d("OpenRecipes", "XmlParser::openFile " + uri.toString());
		Matcher m = new Matcher();
		if (m.match(uri) != Matcher.RECIPE_XML) throw new FileNotFoundException();
		List<String> seg = uri.getPathSegments();
		if (seg.size() != 2 || !seg.get(0).equals("recipe_xml")) throw new FileNotFoundException();
		String recipeXml = Helpers.getRecipeXml(seg.get(1));
		if (recipeXml.length() == 0) throw new FileNotFoundException();
		ParcelFileDescriptor[] fd;
		try {
			fd = ParcelFileDescriptor.createPipe();
		} catch (IOException e) {
			return null;
		}
		writeThread = new WriteThread(fd[1], recipeXml);
		writeThread.start();
		return fd[0];
	}

	class Matcher extends UriMatcher {
		static final int RECIPE_XML = 1;

		public Matcher() {
			super(NO_MATCH);
			addURI("org.jschwab.openrecipes.provider", "recipe_xml/*", RECIPE_XML);
		}
	}

	class WriteThread extends Thread {
		ParcelFileDescriptor fd;
		String recipeXml;

		public WriteThread(ParcelFileDescriptor fd, String recipeXml) {
			this.fd = fd;
			this.recipeXml = recipeXml;
		}

		public void run() {
			FileWriter fw = new FileWriter(fd.getFileDescriptor());
			try {
				fw.write(recipeXml, 0, recipeXml.length());
				fw.close();
				fd.close();
			} catch (IOException e) {
				Log.w("OpenRecipes", "Can't write recipe to pipe");
			}
		}
	}
}
