cd `dirname "${BASH_SOURCE[0]}"`
make distclean
export OPENRECIPES_ANDROID_SYSROOT=`pwd`/androidlibs/arm
export ANDROID_NDK_PLATFORM=android-27
~/Qt/5.11.1/android_armv7/bin/qmake CONFIG+=debug
make clean
make -j5
mkdir client/android_arm
make install INSTALL_ROOT=android_arm
cd client
export TERM=xterm-color
~/Qt/5.11.1/android_armv7/bin/androiddeployqt --output android_arm --gradle --input android-libopenrecipes.so-deployment-settings.json
