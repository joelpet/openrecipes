#! /bin/bash

VERSION=0.2.2
REVISION=1

mkdir build_deb
git archive --prefix=openrecipes-${VERSION}/ --output build_deb/openrecipes_${VERSION}.orig.tar.gz HEAD
cd build_deb
tar -xf openrecipes_${VERSION}.orig.tar.gz
cd openrecipes-${VERSION}
dpkg-buildpackage -S

cd ..
lintian --profile debian openrecipes_${VERSION}-${REVISION}.dsc && echo "lintian .dsc -> ok"
lintian --profile debian openrecipes_${VERSION}-${REVISION}_source.changes && echo "lintian .changes -> ok"
