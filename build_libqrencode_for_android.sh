#!/bin/bash

set -o xtrace

relScriptPath=`dirname "${BASH_SOURCE[0]}"`
scriptPath=`realpath $relScriptPath`
libsPath="${scriptPath}/androidlibs"

LIB_QRENCODE_DIR="libqrencode-4.0.2"
LIB_QRENCODE_TAR="libqrencode-4.0.2.tar.gz"

TARGET_ARM="arm-linux-androideabi"
TARGET_x86="i686-linux-android"

tar -xf $LIB_QRENCODE_TAR || exit 1
cd $LIB_QRENCODE_DIR || exit 1

./autogen.sh || exit 1

export CFLAGS="-D_FORTIFY_SOURCE=2"

oldPATH=$PATH

export PATH=$oldPATH:${scriptPath}/android-ndk-toolchain/arm/bin
./configure --host $TARGET_ARM --prefix ${libsPath}/arm --without-tools || exit 1
make -j5 V=1 || exit 1
make install || exit 1

make clean || exit 1

export PATH=$oldPATH:${scriptPath}/android-ndk-toolchain/x86/bin
./configure --host $TARGET_x86 --prefix ${libsPath}/x86 --without-tools || exit 1
make -j5 V=1 || exit 1
make install || exit 1
